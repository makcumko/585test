<?php

namespace app\models;

use yii\base\NotSupportedException;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $new_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['Name', 'required'],
            ['Name', 'match', 'pattern' => '#^[ a-z0-9_-]+$#i'],
            ['Name', 'unique', 'targetClass' => self::className(), 'message' => \Yii::t('app', 'This username has already been taken.')],
            ['Name', 'string', 'min' => 2, 'max' => 64],

            ['Phone', 'required'],
            ['Phone', 'match', 'pattern' => '#^[ 0-9+\-()]+$#i'],
            ['Phone', 'unique', 'targetClass' => self::className(), 'message' => \Yii::t('app', 'This phone already exists in user base.')],
            ['Phone', 'string', 'min' => 2, 'max' => 64],

            ['Email', 'required'],
            ['Email', 'email'],
            ['Email', 'unique', 'targetClass' => self::className(), 'message' => \Yii::t('app', 'This email address has already been taken.')],
            ['Email', 'string', 'max' => 64],

            ['Role_id', 'integer'],
            ['Role_id', 'default', 'value' => 2],

            ['IsConfirmed', 'integer'],
            ['IsConfirmed', 'default', 'value' => 0],
            ['IsConfirmed', 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Name' => \Yii::t('app', 'Name'),
            'Phone' => \Yii::t('app', 'Phone'),
            'Email' => \Yii::t('app', 'Email'),
            'Role_id' => \Yii::t('app', 'Role'),
            'IsConfirmed' => \Yii::t('app', 'Is confirmed'),
            'new_password' => \Yii::t('app', 'New password'),
        ];
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['Id' => $id, 'IsConfirmed' => 1]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('findIdentityByAccessToken is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['Name' => $username]);
    }


    public function isAdmin() {
        return $this->Role_id == 1;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->AuthKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->AuthKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->PasswordHash);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->PasswordHash = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->AuthKey = \Yii::$app->security->generateRandomString();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
            }
            return true;
        }
        return false;
    }

    /**
     * @param string $token
     * @return static|null
     */
    public static function findByEmailConfirmToken($token)
    {
        return static::findOne(['ConfirmationToken' => $token, 'IsConfirmed' => 0]);
    }

    /**
     * Generates email confirmation token
     */
    public function generateEmailConfirmToken()
    {
        $this->ConfirmationToken = \Yii::$app->security->generateRandomString();
    }

    /**
     * Removes email confirmation token
     */
    public function removeEmailConfirmToken()
    {
        $this->ConfirmationToken = null;
    }

    /**
     * Returns role object
     * @return \yii\db\ActiveQuery
     */
    public function getRole() {
        return $this->hasOne(Role::className(), ['id' => 'Role_id']);
    }
}
