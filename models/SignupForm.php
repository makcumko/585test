<?php

namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $Name;
    public $Email;
    public $Phone;
    public $password;
    public $verifyCode;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['Name', 'required'],
            ['Name', 'match', 'pattern' => '#^[ a-z0-9_-]+$#i'],
            ['Name', 'unique', 'targetClass' => User::className(), 'message' => \Yii::t('app', 'This username has already been taken.')],
            ['Name', 'string', 'min' => 2, 'max' => 64],

            ['Phone', 'required'],
            ['Phone', 'match', 'pattern' => '#^[ 0-9+\-()]+$#i'],
            ['Phone', 'unique', 'targetClass' => User::className(), 'message' => \Yii::t('app', 'This phone already exists in user base.')],
            ['Phone', 'string', 'min' => 2, 'max' => 64],

            ['Email', 'required'],
            ['Email', 'email'],
            ['Email', 'unique', 'targetClass' => User::className(), 'message' => \Yii::t('app', 'This email address has already been taken.')],
            ['Email', 'string', 'max' => 64],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['verifyCode', 'captcha', 'captchaAction' => '/site/captcha'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->Name = $this->Name;
            $user->Email = $this->Email;
            $user->Phone = $this->Phone;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();

            if ($user->save()) {
                Yii::$app->mailer->compose('emailConfirm', ['user' => $user])
                    ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name])
                    ->setTo($this->Email)
                    ->setSubject(\Yii::t('app', 'Email confirmation for ') . Yii::$app->name)
                    ->send();
            }

            return $user;
        }

        return null;
    }
}