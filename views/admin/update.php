<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Update user').': #' . $model->ID . ' ' . $model->Name;
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $errors = $model->getErrors();
    if (!empty($errors)) {
        foreach ($errors as $ctrl=>$err) {
            echo "<div class='alert alert-danger control-{$ctrl}'>".implode("<br/>", $err)."</div>";
        }
    }
    ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Name')->textInput() ?>
    <?= $form->field($model, 'Phone')->textInput() ?>
    <?= $form->field($model, 'Email')->textInput() ?>
    <?= $form->field($model, 'new_password')->passwordInput() ?>
    <?= $form->field($model, 'IsConfirmed')->checkbox(['value' => '1']) ?>

    <?= $form->field($model, 'Role_id')->dropDownList(ArrayHelper::map(\app\models\Role::find()->asArray()->all(), 'ID', 'Title')) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Update'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
