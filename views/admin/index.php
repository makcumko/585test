<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin index';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>User list</h1>

    </div>

    <div class="body-content">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'ID',
                'Name',
                'Phone',
                'Email',
                [
                    'attribute' => 'role.Title',
                    'value' => 'role.Title',
                    'filter' => Html::activeDropDownList($searchModel, 'Role_id', \yii\helpers\ArrayHelper::map(\app\models\Role::find()->asArray()->all(), 'ID', 'Title'),['class'=>'form-control','prompt' => 'Select Role']),
                ],
                [
                    'attribute' => 'IsConfirmed',
                    'filter' => Html::activeCheckbox($searchModel, 'IsConfirmed', ['label' => '']),
                ],


                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                ],

            ],
        ]); ?>

    </div>
</div>
