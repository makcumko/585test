/*
SQLyog Ultimate v11.52 (64 bit)
MySQL - 5.5.38-0ubuntu0.14.04.1 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tbl_roles` */

DROP TABLE IF EXISTS `tbl_roles`;

CREATE TABLE `tbl_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_roles` */

insert  into `tbl_roles`(`ID`,`Title`) values (1,'Администратор'),(2,'Пользователь');

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(64) DEFAULT NULL,
  `Phone` varchar(16) DEFAULT NULL,
  `Email` varchar(64) DEFAULT NULL,
  `Role_id` int(11) NOT NULL DEFAULT '2',
  `IsConfirmed` tinyint(1) DEFAULT '0',
  `ConfirmationToken` varchar(32) DEFAULT NULL,
  `PasswordHash` varchar(255) DEFAULT NULL,
  `AuthKey` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`ID`,`Name`,`Phone`,`Email`,`Role_id`,`IsConfirmed`,`ConfirmationToken`,`PasswordHash`,`AuthKey`) values (1,'admin','911','admin@585test.test',1,1,'MDx1ARDN_CDnXmbhjJU17Ki_ZQ0XLame','$2y$13$NkfUoNu0AaWEIGRIFUli7.ofIrOrDit9rainRu0kfVa41mXXq.IyG','EGNI_rka8_fj_8VVc14kTcENm6c3qVvg'),(2,'user','123456','user@test.test',2,1,'taIgpqsq_XvWvpOBifCdKLbIj2it1NHN','$2y$13$hqae9NspctidRCv37oNrvuFI4t0wLTx2ZDjCUaw5/i67nD2gDPtAC','VX-j5_fn4vrAWIZqIqiMKb9LW25xj313');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
